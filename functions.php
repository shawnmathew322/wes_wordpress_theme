<?php 

/**********************************************
Register and Enqueue Stylesheets and Scripts
**********************************************/

function add_css(){	
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/build/css/bootstrap.css' );
    wp_enqueue_style( 'main', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'add_css' );

function add_js(){
	wp_deregister_script('jQuery');
	
	wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js', false, '2.8.3', false);	
	wp_register_script('vendor', get_template_directory_uri() . '/assets/build/js/vendor.js', false, '', true);
	wp_register_script('main', get_template_directory_uri() . '/assets/build/js/wes.js', false, '', true);

	wp_enqueue_script('modernizr');
	wp_enqueue_script('vendor');
	wp_enqueue_script('main');
}
add_action('wp_enqueue_scripts', 'add_js');

/**********************************************
Post Thumbnail Support
**********************************************/
add_theme_support( 'post-thumbnails' ); 


/**********************************************
Remove p tags used for line breaks
**********************************************/
remove_filter('the_content', 'wpautop');


/**********************************************
Locate Post, Archive Templates in Subfolder
**********************************************/
// Events
function events_archive_template( $template ){
	if( is_post_type_archive('events') ){
		if( $_template = locate_template('includes/events/archive-events.php') ){
			$template = $_template;
		}
	}

	return $template;
}
add_filter( 'template_include', 'events_archive_template' );

function single_event_template( $single_template ){
	global $post;

	if( $post->post_type == 'events' ){
		$single_template = dirname(__FILE__) . 'includes/events/single-event.php';
	}

	return $single_template;
}
add_filter( 'single_template', 'single_event_template' );

?>