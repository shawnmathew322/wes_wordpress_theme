<?php 

/*
Template Name: Home
*/

get_header(); ?>


<?php get_template_part('/includes/homepage/header/home-header'); ?>

<?php get_template_part('/includes/homepage/free-tools'); ?>

<?php get_template_part('/includes/homepage/whats-new'); ?>

<?php get_template_part('/includes/homepage/events'); ?>

<?php get_template_part('/includes/homepage/e-guides'); ?>

<?php get_template_part('/includes/homepage/choose-wes'); ?>

<?php get_template_part('/includes/homepage/testimonials'); ?>




<?php get_footer(); ?>