<?php $query_posts = new WP_Query('posts_per_page=3'); ?> 


<section class='section whats-new'>
	<div class='container'>
		<p class='section__title'>What's <span>New</span></p>

		<div class='posts'>
			<?php 
				if( $query_posts->have_posts() ) : while( $query_posts->have_posts() ) : $query_posts->the_post();

				//Post Tags
				$tag_arr = wp_get_post_tags($post->ID); 				
			?>
				
				<article class='post'>
					<div class='post__img'>
						<img src='<?php echo the_post_thumbnail_url(); ?>'>
					</div>
					<div class='post__title'>
						<p><?php the_title(); ?></p>
						<div class='post__tags'>
							<?php 
								foreach ($tag_arr as $tag) {
									echo '<a class="post__tags__tag" href="' . get_tag_link($tag->term_id) . '">#' . $tag->name . ' </a>';									
								}

							?>
						</div>
					</div>					
				</article><!-- post -->			

			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>			

		</div><!-- posts -->	

	</div><!-- .container -->
</section><!-- whats-new -->