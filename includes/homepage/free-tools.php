<section class='section free-tools'>
	<div class='container'>

		<p class='section__title'>Use Our <span>Free Tools</span></p>

		<div id='tools-slider' class='carousel slide' data-ride='carousel'>
			<!-- Indicators -->
			<ol class='carousel-indicators'>
				<li data-target='#tools-slider' data-slide-to='0' class='active'></li>
				<li data-target='#tools-slider' data-slide-to='1'></li>
				<li data-target='#tools-slider' data-slide-to='2'></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class='carousel-inner'>
				<div class='item active'>
					<div class='tool'>
						<img class='tool__icon' src='<?php echo get_template_directory_uri(); ?>/assets/img/gpa_calc.png' alt=''>
						<a href='#' class='tool__link'>
							<span class='break'>Grade Point</span> Average Calculator
						</a>
					</div>
					<div class='tool'>
						<img class='tool__icon' src='<?php echo get_template_directory_uri(); ?>/assets/img/degree_pre.png' alt=''>
						<a href='#' class='tool__link'>
							<span class='break'>International</span> Degree Preview
						</a>
					</div>
					<div class='tool'>
						<img class='tool__icon' src='<?php echo get_template_directory_uri(); ?>/assets/img/school_finder.png' alt=''>
						<a href='#' class='tool__link'>
							<span class='break'>Business School</span> Finder
						</a>
					</div>				
				</div><!-- .item -->

				<div class='item'>
					<div class='tool'>
						<img class='tool__icon' src='<?php echo get_template_directory_uri(); ?>/assets/img/gpa_calc.png' alt=''>
						<a href='#' class='tool__link'>
							<span class='break'>SLIDE 2 Grade Point</span> Average Calculator
						</a>
					</div>
					<div class='tool'>
						<img class='tool__icon' src='<?php echo get_template_directory_uri(); ?>/assets/img/degree_pre.png' alt=''>
						<a href='#' class='tool__link'>
							<span class='break'>SLIDE 2 International</span> Degree Preview
						</a>
					</div>
					<div class='tool'>
						<img class='tool__icon' src='<?php echo get_template_directory_uri(); ?>/assets/img/school_finder.png' alt=''>
						<a href='#' class='tool__link'>
							<span class='break'>SLIDE 2 Business School</span> Finder
						</a>
					</div>				
				</div><!-- .item -->

				<div class='item'>
					<div class='tool'>
						<img class='tool__icon' src='<?php echo get_template_directory_uri(); ?>/assets/img/gpa_calc.png' alt=''>
						<a href='#' class='tool__link'>
							<span class='break'>SLIDE 3 Grade Point</span> Average Calculator
						</a>
					</div>
					<div class='tool'>
						<img class='tool__icon' src='<?php echo get_template_directory_uri(); ?>/assets/img/degree_pre.png' alt=''>
						<a href='#' class='tool__link'>
							<span class='break'>SLIDE 3 International</span> Degree Preview
						</a>
					</div>
					<div class='tool'>
						<img class='tool__icon' src='<?php echo get_template_directory_uri(); ?>/assets/img/school_finder.png' alt=''>
						<a href='#' class='tool__link'>
							<span class='break'>SLIDE 3 Business School</span> Finder
						</a>
					</div>				
				</div><!-- .item -->


			</div><!-- .carouse-inner -->


			<!-- Controls -->
			<a class='carousel-control left' href='#tools-slider' role='button' data-slide='prev'>
				<div class="carousel_prev"></div>
			</a>
			<a class='carousel-control right' href='#tools-slider' role='button' data-slide='next'>
				<div class="carousel_next"></div>
			</a>

		</div><!-- .carousel -->

	</div><!-- .container -->
</section><!-- .free-tools -->