<div class='apply-now'>
	<div class='container'>

		<div class='intro'>
			<p class='intro__first'>Before you begin,</p>
			<p class='intro__second'>you will need the following:</p>
		</div><!-- intro -->

		<div class='items clearfix'>
			<div class='col-sm-12 col-md-3 item'>
				<p class='item__num'>01</p>
				<p class='item__content'>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, <a href="#">document requirements</a>
					incididunt ut labore et dolore magna aliqua.
				</p>
			</div>
			<div class='col-sm-12 col-md-3 item'>
				<p class='item__num'>02</p>
				<p class='item__content'>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
					incididunt ut labore et dolore magna aliqua.
				</p>
			</div>
			<div class='col-sm-12 col-md-3 item'>
				<p class='item__num'>03</p>
				<p class='item__content'>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
					incididunt ut labore et dolore magna aliqua.
				</p>
			</div>
			<div class='col-sm-12 col-md-3 item'>
				<p class='item__num'>04</p>
				<p class='item__content'>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
					incididunt ut labore et dolore magna aliqua.
				</p>
			</div>
		</div><!-- items -->

		<div class='get-started--wrapper'>
			<a href='#' class='button mob--medium button--lg'>Get Started</a>
		</div>
		
		<img class='close_apply' src='<?php echo get_template_directory_uri(); ?>/assets/img/close_apply.png'>

	</div>
</div>