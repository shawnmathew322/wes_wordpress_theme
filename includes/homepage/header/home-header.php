<header class='header'>
	<div class='container'>
		<div class='col-sm-3 header__cta'>
			<p>Returning Applicants</p>
			<a href="#" class='button button--lg js_sign_in'>Sign In</a>
		</div>
		<div class='col-sm-3 header__cta'>
			<p>New Applicants</p>
			<a href="#" class='button button--lg js_apply_now'>Apply Now</a>
		</div>
	</div>			

	<?php get_template_part('includes/homepage/header/apply-now'); ?>
</header>

<div class='banner'>
	<div class='container'>
		<div class='col-sm-12'>
			<p>For over 40 years, <span class='bold'>WES</span> has enabled  the global mobility of students and professionals.</p>
		</div>
	</div>
</div>