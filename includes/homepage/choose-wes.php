<section class='section choose-wes'>
	<div class='container'>
		<p class='section__title'>Why choose <span>WES</span></p>

		<div class='value__props'>

			<div class='col-sm-12 col-md-4 value__prop'>
				<img class='value__prop__img' src='<?php echo get_template_directory_uri(); ?>/assets/img/reason1.jpg' alt=''>
				<div class='value__prop--content-wrapper'>
					<p class='value__prop__title'>Lorem ipsum dolor sit amet</p>
					<p class='value__prop__sub'>Lorem ipsum dolor sit amet, dicta affert ex sed, vocent insolens phaedrum quo ne.</p>
				</div>
			</div>
			<div class='col-sm-12 col-md-4 value__prop'>
				<img class='value__prop__img' src='<?php echo get_template_directory_uri(); ?>/assets/img/reason2.jpg' alt=''>
				<div class='value__prop--content-wrapper'>
					<p class='value__prop__title'>Lorem ipsum dolor sit amet</p>
					<p class='value__prop__sub'>Lorem ipsum dolor sit amet, dicta affert ex sed, vocent insolens phaedrum quo ne.</p>
				</div>
			</div>
			<div class='col-sm-12 col-md-4 value__prop'>				
				<img class='value__prop__img' src='<?php echo get_template_directory_uri(); ?>/assets/img/reason3.jpg' alt=''>
				<div class='value__prop--content-wrapper'>
					<p class='value__prop__title'>Lorem ipsum dolor sit amet</p>
					<p class='value__prop__sub'>Lorem ipsum dolor sit amet, dicta affert ex sed, vocent insolens phaedrum quo ne.</p>
				</div>
			</div>

		</div>

	</div><!-- .container -->
</section>