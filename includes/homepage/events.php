<section class='section events'>
	<div class='events__content--bg'></div>

	<div class='events__img'></div>

	<div class='container'>
		<div class='col-sm-12 events__content'>
			<p class='section__title section__title--green'>Events</p>

			<div class='event'>
				<p class='event__category'>Education</p>
				<p class='event__date'>OCT 23</p>
				<a href='#' class='event__title'>Study Business in the U.S.</a>
			</div>
			<div class='event'>
				<p class='event__category'>Career</p>
				<p class='event__date'>NOV 13</p>
				<a href='#' class='event__title'>Evaluating Foreign Credentials</a>
			</div>

			<a href='#' class='button button--sm'>More</a>
		</div>

		<div class='events__img--mobile'>
			<img src='<?php echo get_template_directory_uri(); ?>/assets/img/events_img_mob.jpg'>
		</div>

		<div class='col-sm-12 email_upd email_upd--events'>
			<p>Get timely updates from WES</p>
			<?php echo do_shortcode('[contact-form-7 id="18" title="Email Updates"]'); ?>
		</div>

		<div class='col-sm-12 social--wrapper'>			
			<?php get_template_part('includes/social/social'); ?>
		</div>
	</div><!-- .container -->
</section>