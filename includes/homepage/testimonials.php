<?php 
	
	// Get Latest 5 Testimonial Posts
	$tests = get_posts(array(
		'numberposts' => 5,
		'post_type' => 'testimonial',
		'order' => 'DESC',
		'tax_query' => array(
			array(
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => 'featured'
			)
		)
	));

?>

<section class='section testimonials'>
	<div class='container'>

		<div class='col-sm-12 col-md-10 testimonials--wrapper'>
			<div id='quotes-slider' class='carousel slide' data-ride='carousel'>

				<!-- Indicators -->
				<ol class='carousel-indicators quotes'>
					<?php foreach ($tests as $index => $value){ ?>
						<li class='quotes <?php if($index == 0) echo "active"; ?>' data-target='#quotes-slider' data-slide-to='<?php echo $index; ?>'></li>						
					<?php }; ?>
				</ol>

				<div class='carousel-inner'>
					<?php foreach ($tests as $test) : setup_postdata( $test ); ?>					
						<div class='item <?php if( $test === reset($tests) ) echo "active"; ?>'>
							<div class='quote--wrapper'>
								<p class='quote'><?php echo get_post_meta($test->ID,'quote', true); ?></p>
								<p class='quote__author'><?php echo get_post_meta($test->ID,'author', true); ;?></p>
							</div>
						</div>
				
					<?php 
						endforeach; 
						wp_reset_postdata();
					?>					
				</div><!-- .carousel-inner -->

			</div><!-- #quotes-slider -->
		</div><!-- .testimonials--wrapper-->

	</div><!-- .container -->
</section>