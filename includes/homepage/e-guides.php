<section class='section eguides'>
	<div class='container'>
		<p class='section__title'>E-<span>Guides</span></p>

		<div class='col-sm-12'>
			
			<div class='eguides--wrapper'>
				<div class='col-sm-12 col-md-6 eguide'>
					<div class='eguide--wrapper'>				
						<p class='eguide__title'>Lorem ipsum dolor sit amet</p>
						<p class='eguide__excerpt'>
							Lorem ipsum dolor sit amet, dicta affert ex sed, vocent insolens phaedrum quo ne, 
							duo et error petentium. Cu audiam aliquid neglegentur nam.
						</p>
						<p class='eguide__views'>12,334,567 views</p>
						<a href="#" class='button button--sm'>More</a>
					</div><!-- .eguide--wrapper -->
				</div>

				<div class='col-sm-12 col-md-6 eguide'>
					<div class='eguide--wrapper eguide--wrapper__right'>				
						<p class='eguide__title'>Lorem ipsum dolor sit amet</p>
						<p class='eguide__excerpt'>
							Lorem ipsum dolor sit amet, dicta affert ex sed, vocent insolens phaedrum quo ne, 
							duo et error petentium. Cu audiam aliquid neglegentur nam.
						</p>
						<p class='eguide__views'>12,334,567 views</p>
						<a href="#" class='button button--sm'>More</a>
					</div><!-- .eguide--wrapper -->
				</div>

				<div class='col-sm-12 col-md-6 eguide'>
					<div class='eguide--wrapper'>				
						<p class='eguide__title'>Lorem ipsum dolor sit amet</p>
						<p class='eguide__excerpt'>
							Lorem ipsum dolor sit amet, dicta affert ex sed, vocent insolens phaedrum quo ne, 
							duo et error petentium. Cu audiam aliquid neglegentur nam.
						</p>
						<p class='eguide__views'>12,334,567 views</p>
						<a href="#" class='button button--sm'>More</a>
					</div><!-- .eguide--wrapper -->
				</div>

				<div class='col-sm-12 col-md-6 eguide'>
					<div class='eguide--wrapper eguide--wrapper__right'>				
						<p class='eguide__title'>Lorem ipsum dolor sit amet</p>
						<p class='eguide__excerpt'>
							Lorem ipsum dolor sit amet, dicta affert ex sed, vocent insolens phaedrum quo ne, 
							duo et error petentium. Cu audiam aliquid neglegentur nam.
						</p>
						<p class='eguide__views'>12,334,567 views</p>
						<a href="#" class='button button--sm'>More</a>
					</div><!-- .eguide--wrapper -->
				</div>
			</div><!-- eguides--wrapper -->

		</div><!-- col-sm-12 -->

	</div><!-- .container -->
</section>