<div class='container'>
	<div class='footer__menu clearfix'>
		<div class='col-sm-12 col-md-3 footer__col'>
			<div class='col-title--wrapper'>
				<p class='col-title'>About WES</p>
				<span class='glyphicon glyphicon-triangle-right'></span>
			</div>
			<ul>
				<li><a href="#">Mission & Services</a></li>
				<li><a href="#">Clients & Testimonials</a></li>
				<li><a href="#">Work at WES</a></li>
				<li><a href="#">WES Staff</a></li>
				<li><a href="#">News & Media</a></li>
				<li><a href="#">Contact Us</a></li>
				<li><a href="#">WES Canada</a></li>			
			</ul>
		</div>
		<div class='col-sm-12 col-md-3 footer__col'>
			<div class='col-title--wrapper'>
				<p class='col-title'>Credential Evaluation</p>
				<span class='glyphicon glyphicon-triangle-right'></span>
			</div>
			<ul>
				<li><a href="#">About Credential Evaluation</a></li>
				<li><a href="#">Packages & Fees</a></li>
				<li><a href="#">Document Requirements</a></li>
				<li><a href="#">The WES Difference</a></li>
				<li><a href="#">Apply Now</a></li>
				<li><a href="#">My Account</a></li>							
			</ul>
		</div>
		<div class='col-sm-12 col-md-3 footer__col'>
			<div class='col-title--wrapper'>
				<p class='col-title'>Institutions</p>
				<span class='glyphicon glyphicon-triangle-right'></span>
			</div>
			<ul>
				<li><a href="#">Webinars & Training</a></li>
				<li><a href="#">WENR Blog</a></li>
				<li><a href="#">For Higher Ed Professionals</a></li>
				<li><a href="#">For Employers</a></li>
				<li><a href="#">For Practitioners</a></li>
				<li><a href="#">For Licensing Boards</a></li>
				<li><a href="#">For Government Offices</a></li>
				<li><a href="#">Login to Access WES</a></li>			
			</ul>
		</div>
		<div class='col-sm-12 col-md-3 footer__col'>
			<div class='col-title--wrapper'>
				<p class='col-title'>Other Services</p>
				<span class='glyphicon glyphicon-triangle-right'></span>
			</div>
			<ul>
				<li><a href="#">Global Talent Bridge</a></li>
				<li><a href="#">Student Advisor</a></li>				
			</ul>
		</div>		
	</div><!-- .footer__menu -->

	<div class='footer__cta'>
		<div class='col-sm-12 col-md-6 footer__social footer__col'>
			<div class='col-title--wrapper'>
				<p class='col-title'>Connect</p>
				<span class='glyphicon glyphicon-triangle-right'></span>
			</div>
			<?php get_template_part('includes/social/social'); ?>
		</div>
		<div class='col-sm-12 col-md-6 footer__email email_upd email_upd--footer'>
			<p>Weekly Newsletter Sign Up</p>
			<?php echo do_shortcode('[contact-form-7 id="18" title="Email Updates"]'); ?>
		</div>
		<div style='clear:both;'></div>
	</div><!-- .footer__cta -->

	<div class='col-sm-12 footer__links'>
		<ul>
			<li class='copyright'>&copy; <span class='year'></span> World Education Services</li>
			<li class='priv_pol'><a href='#'>Privacy Policy</a></li>
			<li class='terms_cond'><a href='#'>Terms & Conditions</a></li>
			<li class='site_map'><a href='#'>Site Map</a></li>
		</ul>	
	</div>
</div>