<div class='col-sm-12 col-md-3 filter'>
	<div class='module filter__header'>
		<p>Narrow By...</p>
		<p class='clear_filter'>Clear</p>
	</div>

	<div class='module filter__country'>
		<h4>Country</h4>
		<div class='checkbox'>			
			<label> 
				<input type='checkbox' value='US' data-filter='country'>
				United States
			</label>			
		</div>
		<div class='checkbox'>			
			<label> 
				<input type='checkbox' value='Canada' data-filter='country'>
				Canada
			</label>			
		</div>
	</div>

	<div class='module filter__event-type'>
		<h4>Event Type</h4>
		<div class='checkbox'>			
			<label> 
				<input type='checkbox' value='Live_Webinar' data-filter='type'>
				Live Webinar
			</label>			
		</div>
		<div class='checkbox'>			
			<label> 
				<input type='checkbox' value='On-Demand_Webinar' data-filter='type'>
				On-Demand Webinar
			</label>			
		</div>
		<div class='checkbox'>			
			<label> 
				<input type='checkbox' value='Onsite_Seminar' data-filter='type'>
				Onsite Seminar
			</label>			
		</div>
	</div>

	<div class='module filter__topics'>
		<h4>Topics</h4>
		<?php
			foreach ($topics as $topic){
				$topic_class = str_replace(' ', '_', $topic->name);
				echo '<div class="checkbox"><label><input type="checkbox" ';
				echo 'value="' . $topic_class .'" data-filter="topic">' . $topic->name;
				echo '</label></div>';
			}
		?>
	</div>

	<div class='module filter__keywords'>
		<h4>Keywords</h4>
		<?php
			foreach ($keywords as $keyword){
				$kw_class = str_replace(' ', '_', $keyword->name);
				echo '<div class="checkbox"><label><input type="checkbox" ';
				echo 'value="' . $kw_class .'" data-filter="keyword">' . $keyword->name;
				echo '</label></div>';
			}
		?>
	</div>

</div>