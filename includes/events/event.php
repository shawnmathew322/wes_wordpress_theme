<?php
	// String to add as Event Item Class
	$filter_terms = '';

	// Country Value
	$country = get_field('country');	
	if( strcmp($country,'United States') == 0 ){
		$country = 'US';
	}
	$filter_terms = $country . ' ';

	// Event Type Value
	$event_type = get_field('event_type');
	foreach ($event_type as $type) {
		$type = str_replace(' ', '_', $type);
		$filter_terms .= $type . ' ';
	}

	// Get Taxonomy Terms - Topics, Keywords
	$tags = wp_get_post_terms($post->ID, array('topics','keywords'),array('fields' => 'names'));	
	
	foreach ($tags as $tag) {
		$tag = str_replace(' ', '_', $tag);
		$filter_terms .= $tag . ' ';
	}

	// Keywords - used for tags under excerpt
	$keywords = wp_get_post_terms($post->ID, array('keywords'),array('fields' => 'names'));

	// Event Image
	$event_img = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

	// Date
	$date = get_field('date', false, false);
	$date = new DateTime($date);
?>

<div class='event__item <?php echo $filter_terms; ?>'>
	<div class='row'>
		<div class='col-sm-12 col-md-4 event__item__featImg'>
			<img src='<?php echo $event_img; ?>'>
		</div>		
		<div class='col-sm-12 col-md-4 event__item__descr'>
			<p class='title'><?php the_title(); ?></p>		
			<?php if( get_field('sub_title') ){
				echo '<p class="sub_title">' . get_field('sub_title') . '</p>';
			}; ?>
			<p class='excerpt'><?php echo get_the_excerpt(); ?></p>
			<div class='tags'>
				<?php 
					foreach ($keywords as $keyword){
						$kw_class = str_replace(' ', '_', $keyword);					
						if( $keyword !== end($keywords) ){
							echo '<span class="tag ' . $kw_class . '" data-filter="' . $kw_class . '">' . $keyword . '</span>, ';	
						} else {
							echo '<span class="tag ' . $kw_class . '" data-filter="' . $kw_class . '">' . $keyword . '</span>';	
						}												
					}
				?>
			</div>
		</div>		
		<div class='col-sm-12 col-md-4 event__item__info'>
			<p><?php the_field('event_type'); ?></p>

			<?php if( get_field('date') ){
				echo '<p>' . $date->format('l F j') . '</p>';
			}; ?>		

			<?php if( get_field('time') ){
				echo '<p>' . get_field('time') . '</p>';
			}; ?>

			<?php if( get_field('location') ){
				echo '<p>' . get_field('location') . '</p>';
			}; ?>

			<a href='#' class='btn btn-default'>Register</a>
		</div>
	</div><!-- row -->
</div><!-- event__item -->