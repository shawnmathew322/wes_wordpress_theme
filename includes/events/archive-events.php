<?php 

get_header(); 

// Get All Events Posts
$args = array(
	'post_type' => 'events',
	/*'meta_key' => 'date',
	'orderby' => 'meta_value_num',
	'order' => 'DESC'*/
);

$events = new WP_Query( $args );

// Get Post Count
$post_count = $events->post_count;

// Topics Taxonomy - passed into sidebar
$topics = get_terms( array('taxonomy' => 'topics') );

// Keywords Taxonomy - passed into sidebar
$keywords = get_terms( array('taxonomy' => 'keywords') );


?>

<section class='container events__archive'>
	<div class='events__archive__wrapper clearfix'>
		<!-- Sidebar Filter -->
		<?php include(locate_template('includes/events/sidebar.php')); ?>

		<!-- Events List -->
		<div class='col-sm-12 col-md-9 event__item__wrapper'>
			<div class='events__archive__header'>
				<p class='events__archive__header__item events_count'>Showing <?php echo $post_count;?> EVENTS</p>	
				<a href='#' class='events__archive__header__item instl_link'>Looking for Institutional Partner Events?</a>
			</div>			
			
			<?php if ( $events->have_posts() ) : while ( $events->have_posts() ) : $events->the_post(); ?>				
				
				<?php get_template_part('includes/events/event'); ?>
				
			<?php endwhile; else: ?>
				<div class='no_events'>
					<p>Sorry, no upcoming events.</p>
				</div>
			<?php endif; wp_reset_postdata(); ?>
		</div><!-- .events__item__wrapper -->

	</div><!-- .events__archive__wrapper -->
</section>











<?php get_footer(); ?>