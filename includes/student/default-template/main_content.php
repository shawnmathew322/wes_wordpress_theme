<section class='section page-content <?php if( get_field('main_content_title') ) echo "hasTitle"; ?>'>
	<?php if( get_field('main_content_title') ):?>
	<p class='section__title'><?php the_field('main_content_title'); ?></p>
	<?php endif; ?>

	<!-- ITEM 1 -->
	<?php if( get_field('main_content_item_1_image') && get_field('main_content_item_1_headline') ):?>
	<div class='bg_grey '>
		<div class='container item'>
			<div class='col-sm-12 col-md-6 item__img'>			
				<img src='<?php the_field("main_content_item_1_image"); ?>'>
			</div>
			<div class='col-sm-12 col-md-6 item__content'>
				<div class='headline'>					
					<span class='headline__txt'><?php the_field('main_content_item_1_headline'); ?></span>
				</div>
				<div class='copy'>
					<p><?php the_field('main_content_item_1_text'); ?></p>
				</div>

				<?php if( get_field('main_content_item_1_cta_link') ): ?>
				<div class='item__cta'>
					<?php $target_blank = get_field('item_1_cta_link_new_window'); ?>
					<a href='<?php the_field("main_content_item_1_cta_link"); ?>' <?php if( $target_blank !== '' && in_array('True', $target_blank) ){ echo 'target="_blank"';} ?> class='button'><?php the_field("main_content_item_1_cta_text"); ?></a>
				</div>						
				<?php endif; ?>
			</div>
		</div><!-- container -->
		<div class='item__img__mob mobile'>
			<img src='<?php the_field("main_content_item_1_image"); ?>'>
		</div>											
	</div>
	<?php endif; ?>

	<!-- ITEM 2 -->
	<?php if( get_field('main_content_item_3_image') && get_field('main_content_item_3_headline') ):?>
	<div>
		<div class='container item'>
			<div class='col-sm-12 col-md-6 item__content'>
				<div class='headline'>
					<span class='headline__txt'><?php the_field('main_content_item_2_headline'); ?></span>
				</div>
				<div class='copy'>
					<p><?php the_field('main_content_item_2_text'); ?></p>					
				</div>
				<?php if( get_field('main_content_item_2_cta_link') ): ?>
				<div class='item__cta'>		
					<?php $target_blank = get_field('item_2_cta_link_new_window'); ?>					
					<a href='<?php the_field("main_content_item_2_cta_link"); ?>' <?php if( $target_blank !== '' && in_array('True', $target_blank) ){ echo 'target="_blank"';} ?> class='button'><?php the_field("main_content_item_2_cta_text"); ?></a>
				</div>
				<?php endif; ?>
			</div>
			<div class='col-sm-12 col-md-6 item__img item__img--right'>
				<img src='<?php the_field("main_content_item_2_image"); ?>'>
			</div>
		</div><!-- container -->
		<div class='item__img__mob mobile'>
			<img src='<?php the_field("main_content_item_2_image"); ?>'>
		</div>				
	</div>
	<?php endif; ?>

	<!-- ITEM 3 -->
	<?php if( get_field('main_content_item_3_image') && get_field('main_content_item_3_headline') ):?>
	<div class='bg_grey'>
		<div class='container item'>
			<div class='col-sm-12 col-md-6 item__img'>
				<img src='<?php the_field("main_content_item_3_image"); ?>'>
			</div>
			<div class='col-sm-12 col-md-6 item__content'>
				<div class='headline'>
					<span class='headline__txt'><?php the_field('main_content_item_3_headline'); ?></span>
				</div>
				<div class='copy'>
					<p><?php the_field('main_content_item_3_text'); ?></p>					
				</div>
				<?php if( get_field('main_content_item_3_cta_link') ): ?>
				<div class='item__cta'>
					<?php $target_blank = get_field('item_3_cta_link_new_window'); ?>					
					<a href='<?php the_field("main_content_item_3_cta_link"); ?>' <?php if( $target_blank !== '' && in_array('True', $target_blank) ){ echo 'target="_blank"';} ?> class='button'><?php the_field("main_content_item_3_cta_text"); ?></a>
				</div>
				<?php endif; ?>
			</div>
		</div><!-- container -->

		<div class='item__img__mob mobile'>
			<img src='<?php the_field("main_content_item_3_image"); ?>'>
		</div>				
	</div>
	<?php endif; ?>

	<!-- ITEM 4 -->
	<?php if( get_field('main_content_item_4_image') && get_field('main_content_item_4_headline') ):?>
	<div>
		<div class='container item'>
			<div class='col-sm-12 col-md-6 item__content'>
				<div class='headline'>
					<span class='headline__txt'><?php the_field('main_content_item_4_headline'); ?></span>
				</div>
				<div class='copy'>
					<p><?php the_field('main_content_item_4_text'); ?></p>					
				</div>
				<?php if( get_field('main_content_item_4_cta_link') ): ?>
				<div class='item__cta'>		
					<?php $target_blank = get_field('item_4_cta_link_new_window'); ?>					
					<a href='<?php the_field("main_content_item_4_cta_link"); ?>' <?php if( $target_blank !== '' && in_array('True', $target_blank) ){ echo 'target="_blank"';} ?> class='button'><?php the_field("main_content_item_4_cta_text"); ?></a>
				</div>
				<?php endif; ?>
			</div>
			<div class='col-sm-12 col-md-6 item__img item__img--right'>
				<img src='<?php the_field("main_content_item_4_image"); ?>'>
			</div>
		</div><!-- container -->
		<div class='item__img__mob mobile'>
			<img src='<?php the_field("main_content_item_4_image"); ?>'>
		</div>				
	</div>
	<?php endif; ?>

	<!-- ITEM 5 -->
	<?php if( get_field('main_content_item_5_image') && get_field('main_content_item_5_headline') ):?>
	<div class='bg_grey'>
		<div class='container item'>
			<div class='col-sm-12 col-md-6 item__img'>
				<img src='<?php the_field("main_content_item_5_image"); ?>'>
			</div>
			<div class='col-sm-12 col-md-6 item__content'>
				<div class='headline'>
					<span class='headline__txt'><?php the_field('main_content_item_5_headline'); ?></span>
				</div>
				<div class='copy'>
					<p><?php the_field('main_content_item_5_text'); ?></p>					
				</div>
				<?php if( get_field('main_content_item_5_cta_link') ): ?>
				<div class='item__cta'>
					<?php $target_blank = get_field('item_5_cta_link_new_window'); ?>					
					<a href='<?php the_field("main_content_item_5_cta_link"); ?>' <?php if( $target_blank !== '' && in_array('True', $target_blank) ){ echo 'target="_blank"';} ?> class='button'><?php the_field("main_content_item_5_cta_text"); ?></a>
				</div>
				<?php endif; ?>
			</div>
		</div><!-- container -->

		<div class='item__img__mob mobile'>
			<img src='<?php the_field("main_content_item_5_image"); ?>'>
		</div>				
	</div>
	<?php endif; ?>

	<!-- ITEM 6-->
	<?php if( get_field('main_content_item_6_image') && get_field('main_content_item_6_headline') ):?>
	<div>
		<div class='container item'>
			<div class='col-sm-12 col-md-6 item__content'>
				<div class='headline'>
					<span class='headline__txt'><?php the_field('main_content_item_6_headline'); ?></span>
				</div>
				<div class='copy'>
					<p><?php the_field('main_content_item_6_text'); ?></p>					
				</div>
				<?php if( get_field('main_content_item_6_cta_link') ): ?>
				<div class='item__cta'>		
					<?php $target_blank = get_field('item_6_cta_link_new_window'); ?>					
					<a href='<?php the_field("main_content_item_6_cta_link"); ?>' <?php if( $target_blank !== '' && in_array('True', $target_blank) ){ echo 'target="_blank"';} ?> class='button'><?php the_field("main_content_item_6_cta_text"); ?></a>
				</div>
				<?php endif; ?>
			</div>
			<div class='col-sm-12 col-md-6 item__img item__img--right'>
				<img src='<?php the_field("main_content_item_6_image"); ?>'>
			</div>
		</div><!-- container -->
		<div class='item__img__mob mobile'>
			<img src='<?php the_field("main_content_item_6_image"); ?>'>
		</div>				
	</div>
	<?php endif; ?>

	<!-- ITEM 7 -->
	<?php if( get_field('main_content_item_7_image') && get_field('main_content_item_7_headline') ):?>
	<div class='bg_grey'>
		<div class='container item'>
			<div class='col-sm-12 col-md-6 item__img'>
				<img src='<?php the_field("main_content_item_7_image"); ?>'>
			</div>
			<div class='col-sm-12 col-md-6 item__content'>
				<div class='headline'>
					<span class='headline__txt'><?php the_field('main_content_item_7_headline'); ?></span>
				</div>
				<div class='copy'>
					<p><?php the_field('main_content_item_7_text'); ?></p>					
				</div>
				<?php if( get_field('main_content_item_7_cta_link') ): ?>
				<div class='item__cta'>
					<?php $target_blank = get_field('item_7_cta_link_new_window'); ?>					
					<a href='<?php the_field("main_content_item_7_cta_link"); ?>' <?php if( $target_blank !== '' && in_array('True', $target_blank) ){ echo 'target="_blank"';} ?> class='button'><?php the_field("main_content_item_7_cta_text"); ?></a>
				</div>
				<?php endif; ?>
			</div>
		</div><!-- container -->

		<div class='item__img__mob mobile'>
			<img src='<?php the_field("main_content_item_7_image"); ?>'>
		</div>				
	</div>
	<?php endif; ?>

</section>