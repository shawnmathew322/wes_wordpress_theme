<?php 
	
	$bannerImg = get_field("intro_item_image");
	$bannerContent = get_field("intro_item_headline") || get_field('intro_item_text');
?>

<section class='section intro'>
	<div class='container'>
		<div class='col-sm-12'>
			<h1 class='section__title intro__title'><?php the_field('intro_title'); ?></h1>
			<div class='intro__content'><?php the_field('intro_text'); ?></div>			
		</div>
	</div>
</section>

<?php if( $bannerImg || $bannerContent ): ?>
<section class='banner'>
	<?php if( $bannerImg && $bannerContent ):?>	
		<div class='banner__img' style='background: url(<?php the_field("intro_item_image"); ?>) no-repeat; background-size: cover;'></div>	
	<?php elseif( $bannerImg && !$bannerContent ): ?>
		<img class='banner__img--fullWidth' src='<?php the_field("intro_item_image"); ?>'>
	<?php endif; ?>

	<?php if( $bannerContent ):?>
	<div class='banner__content <?php if( !$bannerImg ) echo "banner__content--fullWidth container"; ?>'>
		<p class='title <?php if( !$bannerImg ) echo "title--fullWidth"; ?>'><?php the_field('intro_item_headline'); ?></p>
		<div class='copy <?php if( !$bannerImg ) echo "copy--fullWidth"; ?>'><?php the_field('intro_item_text'); ?></div>
	</div>
	<?php endif; ?>

	<?php if( $bannerImg ):?>
	<div class='banner__img__mob mobile'>
		<img src='<?php the_field("intro_item_image"); ?>'>
	</div>
	<?php endif; ?>
</section>
<?php endif; ?>