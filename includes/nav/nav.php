<div class="nav">
	
	<!-- DESKTOP NAV -->
	<div class="desktop">
	
		<div class="nav__top">
			<div class="container">
				<ul>
					<li><a href="javascript:;">Sign In</a></li>
					<li><a href="javascript:;">Apply now</a></li>
					<li class="main--drop language">
						<a href="javascript:;" class="main--drop--selected">English <span class='drop_arrow'></span></a>
						<a href="javascript:;" class="main--drop--option">French <span class='drop_arrow'></span></a>
					</li>
					<li class="main--drop country">
						<a href="javascript:;" class="main--drop--selected"><span class="us-flag"></span>WES United States <span class='drop_arrow'></span></a>
						<a href="javascript:;" class="main--drop--option"><span class="ca-flag"></span>WES Canada <span class='drop_arrow'></span></a>
					</li>
				</ul>
			</div>
		</div>		
		
		<div class="nav__main">
			<div class="container">
				<a href="/">
					<img class="logo" src="/wp-content/themes/wes/assets/img/wes_logo.png" />
				</a>
				<ul>
					<li>
						<a href="javascript:;" class="main-nav-item">Credential Evaluation</a>
						<ul>
							<li><a href="#">Apply Now</a></li>
							<li><a href="#">Evaluation and Fees</a></li>
							<li><a href="#">Document Requirements</a></li>
							<li><a href="#">About Credential Evaluation</a></li>
							<li><a href="#">The WES Difference</a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:;" class="main-nav-item">Tools &amp; Advice</a>
						<ul>
							<li><a href="#">Apply Now</a></li>
							<li><a href="#">Evaluation and Fees</a></li>
							<li><a href="#">Document Requirements</a></li>
							<li><a href="#">About Credential Evaluation</a></li>
							<li><a href="#">The WES Difference</a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:;" class="main-nav-item">Help Center</a>
						<ul>
							<li><a href="#">Apply Now</a></li>
							<li><a href="#">Evaluation and Fees</a></li>
							<li><a href="#">Document Requirements</a></li>
							<li><a href="#">About Credential Evaluation</a></li>
							<li><a href="#">The WES Difference</a></li>
						</ul>
					</li>
					<li>
						<a href="javascript:;" class="main-nav-item">Institutions</a>
						<ul>
							<li><a href="#">Apply Now</a></li>
							<li><a href="#">Evaluation and Fees</a></li>
							<li><a href="#">Document Requirements</a></li>
							<li><a href="#">About Credential Evaluation</a></li>
							<li><a href="#">The WES Difference</a></li>
						</ul>
					</li>
				</ul>
				
				<div class="nav__search"></div>
				
				<div class="nav__search__container">
					<?php get_template_part('/includes/search/desktop-search'); ?>
				</div>							
			</div>
		</div>
	</div>
	
	
	<!-- MOBILE NAV -->
	<div class="mobile">
		<div class="logo">
			<img class="logo--mobile" src="/wp-content/themes/wes/assets/img/wes_logo.png" />
			<div class="bar">
				<div class="hamburger"></div>
				<div class="mobile__search"></div>
								
				<?php get_template_part('/includes/search/mobile-search'); ?>

			</div>
			<div class="nav__mobile">
				<ul class='scrollable'>
					<li><a href="#" class="drop">U.S.</a>
						<ul>
							<li><a href="#">Canada</a></li>
						</ul>
					</li>
					<li>
						<a href="#" class="drop">English</a>
						<ul>
							<li><a href="#">French</a></li>
						</ul>
					</li>
					<li><a href="#">Sign In</a></li>
					<li><a href="#">Apply Now</a></li>
					<li>
						<a href="#" class="drop">Credential Evaluation</a>
						<ul>
							<li><a href="#">Apply Now</a></li>
							<li><a href="#">Evaluation and Fees</a></li>
							<li><a href="#">Document Requirements</a></li>
							<li><a href="#">About Credential Evaluation</a></li>
							<li><a href="#">The WES Difference</a></li>
						</ul>
					</li>
					<li>
						<a href="#" class="drop">About WES</a>
						<ul>
							<li><a href="#">Mission & Services</a></li>
							<li><a href="#">Clients & Testimonials</a></li>
							<li><a href="#">Work at WES</a></li>
							<li><a href="#">WES Staff</a></li>
							<li><a href="#">News & Media</a></li>
							<li><a href="#">Contact Us</a</li>
							<li><a href="#">WES Canada</a></li>						
						</ul>
					</li>
					<li>
						<a href="#" class="drop">Institutions</a>
						<ul>
							<li><a href="#">Webinars & Training</a></li>
							<li><a href="#">WENR Blog</a></li>
							<li><a href="#">For Higher Ed Professionals</a></li>
							<li><a href="#">For Employers</a></li>
							<li><a href="#">For Practitioners</a></li>
							<li><a href="#">For Licensing Boards</a></li>
							<li><a href="#">For Government Offices</a></li>
							<li><a href="#">Log into Access WES</a></li>
						</ul>
					</li>
					<li>
						<a href="#" class='drop'>Other Services</a>
						<ul>
							<li><a href="#">Global Talent Bridge</a></li>
							<li><a href="#">Student Advisor</a></li>						
						</ul>
					</li>
					<li>
						<a href="#" class='drop'>Connect</a>
						<ul>
							<li><a href="#">Global Talent Bridge</a></li>
							<li><a href="#">Student Advisor</a></li>
						</ul>
					</li>
					<li class="type--size">Type Size <a htrf="#" class="sm">A</a> <a htrf="#" class="md">A</a> <a htrf="#" class="lg">A</a></li>
					<li class="mobile--social">
						<a class="twitter" href="#"></a>
						<a class="linkedin" href="#"></a>
						<a class="blog" href="#"></a>
						<a class="facebook" href="#"></a>
						<a class="youtube" href="#"></a>
						<a class="vimeo" href="#"></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
</div>