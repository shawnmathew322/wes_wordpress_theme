<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">		
		<meta name="description" content="World Education Services">
		<title>World Education Services</title>
		<?php wp_head(); ?>
	</head>
	<body>
		
		<?php get_template_part('includes/nav/nav'); ?>

		<div class='main_wrapper'>		

		