module.exports = function(grunt){

	"use strict";
   	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
	    	build: {
	        	files: {
	            	'style.css': 'assets/sass/master.scss'
	        	}
	    	}
		},

        concat: {
            options: {
                separator: ';\n',
            },
            vendor: {
                src: [
                    'node_modules/jquery/dist/jquery.min.js',
                    'assets/js/vendor/jquery.mobile.custom.min.js',
                    'node_modules/bootstrap/dist/js/bootstrap.min.js' 
                ],
                dest: 'assets/build/js/vendor.js'
            },            
            js: {
                src: [
                    'assets/js/global/nav.js',  
                    'assets/js/global/carousels.js',
                    'assets/js/global/email_input.js',
                    'assets/js/global/footer.js',

                    'assets/js/student/homepage/header.js',                      
                    'assets/js/student/search/homepage/search.js',
                    'assets/js/student/events/filter.js',
                    
                    'assets/js/wes.js'
                ],
                dest: 'assets/build/js/wes.js'
            },
            vendor_css: {
                src: [
                    'node_modules/bootstrap/dist/css/bootstrap.css'
                ],
                dest: 'assets/build/css/bootstrap.css'
            }            
        },

        watch: {
        	css: {
                files: ['assets/sass/**/*.scss'],
                tasks: ['sass', 'concat']
            },
            js: {
                files: ['assets/js/**/*.js'],
                tasks: ['concat']
            }
        },
    });

    grunt.registerTask('default', [
        'sass',
        'concat'
    ]);

};