var Filter_Events = (function(){

	var events = $('.event__item'),
		filter_terms_obj = {},
		filter_terms_arr = [],
		filter_terms = '',
		remove_term = false,
		filter_type = '',
		multi_term_type_filter = false,
		filter_type_count = 1;

	var init = function(){			

		// Prevent Selection of both countries
		$('[data-filter="country"]').on('click', function(e){
			if( $('[data-filter="country"]:checked').length > 1 ){
				e.preventDefault();
			} else {
				return;
			}
		});

		// Checkbox Filter
		$('.filter input[type="checkbox"]').on('change', function(){			

			var filter_value = $(this).attr('value'),
				filter_type = $(this).data('filter');

			if( $(this).is(':checked') ){
				remove_term = false;
				filter_sort(filter_value, filter_type);
			} else {
				remove_term = true;
				filter_sort(filter_value, filter_type);
			}
						
			events_sort();
		});		

		// Keyword Filter
		$('.tag').on('click', function(){
			// Get Value for Sort
			var filter_value = $(this).data('filter'),
				filter_type = $(this).data('filter');
			
			// Select Checkbox, Trigger Change Event
			$('[value="' + filter_value + '"]').prop('checked', true).trigger('change');
			
		});

		// Clear Filters
		$('.clear_filter').on('click', function(){
			$('.filter input[type="checkbox"]').prop('checked', false);
			filter_terms = '';
			events_sort();

			multi_term_type_filter = false;
		});
 
	};


	function filter_sort(filter_value, filter_type){		

		filter_value = '.' + filter_value;		

		if( remove_term == false ){												
			update_filter_terms_arr(filter_value, filter_type);
			update_filter_terms();
		} else {
			update_filter_terms_arr(filter_value, filter_type);
			update_filter_terms();
		}

		console.log(filter_terms);
	}


	function update_filter_terms_arr(filter_value, filter_type){				

		// Add to filter_terms		
		if( remove_term == false ){

			// if filter terms !empty - append current filter value to each filter term based on filter type
			if( filter_terms_arr.length > 0 ){
				// Determine if multi type, multi term filter by 
				// checking if obj props != current filter type have values
				for(var prop in filter_terms_obj){
					if( prop !== filter_type ){
						if( filter_terms_obj[prop].length > 0 ){
							multi_term_type_filter = true;
							filter_type_count++;
						}
					}
				}

				// if filter_terms_obj has filter_type property
				if( filter_terms_obj.hasOwnProperty(filter_type) ){
					add_filter_terms_arr(filter_value, filter_type);
				}
				// If filter_terms_obj does not have filter_type property 
				else {
					filter_terms_arr.forEach(function(term, index, filter_terms_arr){
						filter_terms_arr[index] = filter_value + term;				
					});
				}						
			} else {
				// filter_terms = empty, just add current filter value
				
				// Update Terms Array
				filter_terms_arr.push(filter_value);	
			}

			// Add Update Terms Object
			update_filter_terms_obj(filter_value, filter_type);
		} 

		// Remove from filter_terms
		else {
			// Remove Value from Terms Object Property
			update_filter_terms_obj(filter_value, filter_type);

			// Remove Term from Terms Array
			remove_filter_terms_arr(filter_value, filter_type);

		}// remove from filter_terms
		
		console.log(filter_terms_obj);

	}// update_filter_terms_arr


	/****************************************************
	 Update filter terms array based on filter 
	 term object property values & if multi term filter
	*****************************************************/
	function add_filter_terms_arr(filter_value, filter_type){
		// If Multi Term & Current Filter Type object property has values
		if( filter_terms_obj[filter_type].length > 0 && multi_term_type_filter ){			
			if( filter_type_count > 2 ){
				filter_terms_arr.forEach(function(term){
					// If term contains value from current filter value's filter type
					if( term.indexOf(filter_terms_obj[filter_type][0]) > -1 ){
						// Add a new term by replacing first instance of shared filter type value
						addNewTerm = term.replace(filter_terms_obj[filter_type][0], filter_value);
						filter_terms_arr.push( addNewTerm );
					}
				});				
			} else {
				for(var prop in filter_terms_obj){							
					if( prop !== filter_type ){
						filter_terms_obj[prop].forEach(function(el){
							filter_terms_arr.push( filter_value + el );
						});																
					}
				}	
			}						
		} else if( filter_terms_obj[filter_type].length < 1 && multi_term_type_filter ){
			// If Multi Term & Current Filter Type object property does not have values
			filter_terms_arr.forEach(function(term, index, filter_terms_arr){
				filter_terms_arr[index] = filter_value + term;				
			});	
		} else {
			// If !multi-term, add current filter value to terms array
			filter_terms_arr.push(filter_value);
		}	
	};


	/****************************************************
	 Remove Term From Filter Terms Array
	*****************************************************/
	function remove_filter_terms_arr(filter_value, filter_type){
		var remove_filter_terms = [],
			is_single_type = false;

		// Check value against each term in fiter_terms_arr & update filter_terms_arr			
		filter_terms_arr.forEach(function(term, index, filter_terms_arr){
			if( term.indexOf(filter_value) > -1 ){
				
				// Set var for check if reduced terms are all in one filter type		
				for(var prop in filter_terms_obj){
					// Check current filter_value type array length						
					if( filter_terms_obj[filter_type].length < 1 ){
						// Check if other filter types are populated
						if( prop !== filter_type ){
							if( filter_terms_obj[prop].length > 0 ){							
								is_single_type = true;
								multi_term_type_filter = false;			
								filter_type_count = 1;						
							}	
						}	
					}						
				}

				// Check if filter terms are still multi type
				if( !is_single_type ){
					remove_filter_terms.push(term);
				} else {
					filter_terms_arr[index] = term.replace(filter_value, '');
				}										
			}// term.indexOf(filter_value)												
		});			

		// If multi type filter, splice filter_terms_arr using remove terms values
		if( !is_single_type ){
			for( var i = remove_filter_terms.length - 1; i >= 0; i-- ){
				var remove_term_indx = filter_terms_arr.indexOf(remove_filter_terms[i]);
				filter_terms_arr.splice( remove_term_indx, 1 );
			}	
		}
	};


	/****************************************************
	 Add to filter terms object 	 
	*****************************************************/
	function update_filter_terms_obj(filter_value, filter_type){
		// Add to Terms Object
		if( remove_term == false ){
			if( !filter_terms_obj.hasOwnProperty(filter_type) ){
				filter_terms_obj[filter_type] = [];				
			}
			filter_terms_obj[filter_type].push(filter_value);

		} else {
			// Remove
			var upd_obj = filter_terms_obj[filter_type].indexOf(filter_value);
			filter_terms_obj[filter_type].splice(upd_obj, 1);
		}			
	};


	/****************************************************
	 Update filter terms string 	 
	*****************************************************/
	function update_filter_terms(){
		filter_terms = '';
		
		filter_terms_arr.forEach(function(term, index){						
			filter_terms += ( filter_terms !== '' ) ? ',' + term : term;			
		});
	};


	/****************************************************
	 Sort event items
	*****************************************************/
	function events_sort(){
		events.fadeOut('fast');

		if( filter_terms !== '' ){
			events.filter(filter_terms).fadeIn('slow');	
		} else {
			events.fadeOut('fast').fadeIn('slow');
		}
	};



	return {
		init: init
	}


})();