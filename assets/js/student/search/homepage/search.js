var Search = (function(){

	var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;


	var init = function(){
		// Toggle Search Display
		toggle_search();	

		// Resize - on desktop, remove opaque display		
		$(window).resize(function(){
			if( $(window).width() > 959 ){
				$(".mobile__search__box").hide();	
				$('header, .banner, section, footer').hasClass('opaque') ? $('header, .banner, section, footer').removeClass('opaque') : null;						
			}
		});
	};


	function toggle_search(){

		$(".nav__search, .nav--search--close .btn").on("click",function(){
			$(".nav__search__container").toggle();
			// hide nav drop downs
			$(".nav__main ul li ul").hide();
		});

		$(".mobile__search, .mobile--search--close").on("click",function(){
			$(".mobile__search__box").toggle();
			on_search_display();

			// Prevent body scrolling
			$('html, body').toggleClass('no_scroll');			
			if( iOS ){
				if( $('.mobile__search__box').is(':visible') ){
					$(document).on('touchmove', function(e){
						e.preventDefault();
					});		
				} else {
					$(document).unbind('touchmove');
				}		
			}
		});
	}


	function on_search_display(){
		$('header, .banner, section, footer').toggleClass('opaque');
	}

	return {
		init: init
	}


})();