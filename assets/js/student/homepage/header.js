var Header = (function(){

	var ua = navigator.userAgent.toLowerCase(),
		isAndroid = ( ua.indexOf('android') > -1 ) ? true : false;


	var init = function(){
		// Apply Now
		$('.js_apply_now').on('click', function(){
			$('.apply-now').show();
			prevent_body_scroll();
		});

		$('.close_apply').on('click', function(){
			$('.apply-now').hide();
			$('html, body').removeClass('no_scroll')
			$(document).unbind('touchmove');
		});

		// Toggle scroll prevention depending on width & device
		$(window).resize(function(){
			if( $(window).width() > 959 && $('html, body').hasClass('no_scroll') ){
				$('html, body').removeClass('no_scroll');				
				$(document).unbind('touchmove');
			} else {
				if( $('.apply-now').is(':visible') ){
					prevent_body_scroll();
				}
			}
		});
	};


	function prevent_body_scroll(){
		if( $(window).width() < 960 ){
			$('html, body').addClass('no_scroll');

			if( isAndroid ){
				$(document).on('touchmove', function(e){
					e.preventDefault();
				});
			}
		}		
	}


	return {
		init: init
	}



})();