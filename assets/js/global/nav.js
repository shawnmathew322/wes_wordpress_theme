var Nav = (function(){

	var nav_is_fixed,
		$mBar = $(".nav .mobile .bar"),
		$mMenu = $(".nav__mobile"),

		ua = navigator.userAgent.toLowerCase(),
		isAndroid = ( ua.indexOf('android') > -1 ) ? true : false,
		iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,

		currentScrollPos;

		if( iOS ){
			$(window).scroll(function(){
				currentScrollPos = $(document).scrollTop();
			});			
		}


	var init = function(){		

		/****************************
			Fixed Nav
		*****************************/
		checkMobileNav();
		$(window).scroll(function(){
			checkMobileNav();
		});		


		/****************************
			Top Nav Click
		*****************************/
		BindDropClick();


		/****************************
			Main Nav Click
		*****************************/		
		main_nav_click();


		/*****************************
			Hide Dropdowns, Searchform on Resize
		*****************************/		
		$(window).resize(function(){
			$(".main--drop").removeClass("drop--open");
			$(".nav__main ul li ul").hide();

			//hide menus & search	
			if( !isAndroid ){											
				$(".nav__search__container").hide();				
			} else {
				if( !$('.nav--search').is(':focus') ){
					$(".nav__search__container").hide();
				}
			}
		});
				

		/****************************
			Mobile Click 
		*****************************/		
		mobile_click_events();

	};



	function main_nav_click(){
		$(".main-nav-item").on("click",function(){
			var $item = $(this).parent().find("ul").eq(0);
			$(".nav__main ul li ul").not($item).hide();
			$item.toggle();			
			
			//hide search
			$(".nav__search__container").hide();			
		});

		$(document).on('click', function(e){
			if( $(e.target).parents('.nav__main').length == 0 ){
				$(".nav__main ul li ul").hide();
			}
		});	
	}	
		

	function checkMobileNav(){
		var sTop = $(window).scrollTop();
		if(sTop >= 114 && !$mBar.hasClass("mobile--top")){
			$mBar.addClass("mobile--top");
			$mMenu.addClass("mobile--top");

			nav_is_fixed = true;

			if( $('.nav__mobile').is(':visible') ){
				$('html, body').addClass('no_scroll');
				$('.main_wrapper').addClass('opaque--nav_display');
			}			
		}
		else if(sTop < 114 && $mBar.hasClass("mobile--top")){
			$mBar.removeClass("mobile--top");
			$mMenu.removeClass("mobile--top");

			nav_is_fixed = false;
			
			if( $('.nav__mobile').is(':visible') ){
				$('html, body').removeClass('no_scroll');						
				$('.main_wrapper').removeClass('opaque--nav_display');
			}			
		}
	}


	function BindDropClick(){	
		// Display sub link
		$('body').on('click', '.main--drop--selected', function(){			
			var sub_link = $(this).siblings('.main--drop--option');
			
			if( $('.main--drop--option').not(sub_link).hasClass('show') ){
				$('.main--drop--option').removeClass('show')
			}

			sub_link.toggleClass('show');
		});		

		// Sub link click
		$('body').on('click', '.main--drop--option.show', function(){
			var orig_display_link = $(this).parent().children('.main--drop--selected');

			 orig_display_link.removeClass('main--drop--selected').addClass('main--drop--option');
			 $(this).removeClass('main--drop--option show').addClass('main--drop--selected');			
		});

		// Hide Sub link on off click
		$(document).on('click', function(e){
			if( e.target.className !== 'main--drop--selected' ){
				$('.main--drop--option').removeClass('show');
			} 
		});
	}
	

	function mobile_click_events(){
		// Hamburger Click
		$('body').on("click", ".hamburger", function(){
			// Toggle Nav Display
			$(".nav__mobile").toggle();
			$(this).toggleClass("openNav");

			// Prevent body scroll && iOS specific logic
			if( $(this).hasClass('openNav') ){				
				$('.main_wrapper').addClass('opaque--nav_display');

				if( nav_is_fixed ){
					$('html, body').addClass('no_scroll');				
				}

				if( iOS ){
					$('.main_wrapper').addClass('opaque--nav_display');
					removeIOSRubberEffect();
				}
			} else {
				$('html, body').removeClass('no_scroll');				
				$('.main_wrapper').removeClass('opaque--nav_display');
			}
			
		});
		
		// Nav Item Click
		$(".nav__mobile a.drop").on("click",function(e){
			e.preventDefault();

			var $drop = $(this).parent().find("ul").eq(0);
			$drop.toggle();
			$(".nav__mobile ul ul").not($drop).hide();
			$(this).toggleClass("openDrop");
			$("a.drop").not($(this)).removeClass("openDrop");
		});
	}


	function removeIOSRubberEffect() {
     	$('.scrollable').on('touchstart', function(){
     		var top = $(this).scrollTop,
     			totalScroll = $(this).scrollHeight,
     			currentScroll = top + $(this).offsetHeight;
 
 			if ( top === 0 ) {
 	            $(this).scrollTop = 1;
 	        } else if ( currentScroll === totalScroll ) {
 	            $(this).scrollTop = top - 1;
 	        }
     	});
 	}


 	/***********************
 		TEMP REMOVED - do not use as fn call
 	************************/ 
 	function ios_prevent_scrolling(){
 		if( iOS ){
			if( $(this).hasClass('openNav') ){
				$('.logo--mobile').hide();				

				$('.main_wrapper')
					.css({
						'position': 'fixed'
					})
					.addClass('opaque--nav_display');

				removeIOSRubberEffect();

				localStorage.setItem('cachedScrollPos', currentScrollPos);
			} else {				
				$('.logo--mobile').show();
				$('.main_wrapper')
					.css({					
						'position': 'relative'
					})
					.removeClass('opaque--nav_display');
				
				$('body').scrollTop( localStorage.getItem('cachedScrollPos') );
			}
		}		
 	}


	return {
		init: init
	}

})();



