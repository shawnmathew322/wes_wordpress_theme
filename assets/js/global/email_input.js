var Email_Input = (function(){

	var $email_events = $('.email_upd--events .wpcf7-form input[type="email"]'),
		$email_footer = $('.email_upd--footer .wpcf7-form input[type="email"]');

	var init = function(){

		// Email Input form
		email_input_size()		

		$(window).resize(function(){
			email_input_size()			
		});		

	};


	function email_input_size(){
		// Set initial email footer input size
		$email_footer.attr('size', '20');

		if( $(window).width() > 1380 ){
			$email_events.attr('size', '40');				
		} else if( $(window).width() < 1380 && $(window).width() > 1140 ){
			$email_events.attr('size', '30');		
		} else {
			$email_events.attr('size', '20');			
		}
	};

	return {
		init: init
	}

})();