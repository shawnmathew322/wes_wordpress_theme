var Footer = (function(){

	var init = function(){
		
		/**************************
			Footer Menu Dropdowns
		***************************/
		menu_toggle();
		$(window).resize(function(){
			if( $(window).width() < 832 ){					
				// Remove clear from div to prevent extra spacing
				$('.footer__menu').removeClass('clearfix');

				// Hide any previously displayed sub menu's & toggle glyphs
				if( $('.footer__col ul').hasClass('show') ){
					$('.footer__col ul').removeClass('show')
				}

				if( $('.col-title--wrapper > .glyphicon').hasClass('glyphicon-triangle-bottom') ){
					$('.col-title--wrapper > .glyphicon')
						.removeClass('glyphicon-triangle-bottom')
						.addClass('glyphicon-triangle-right');
				}
			} else {
				// Add back clear on footer menu div
				$('.footer__menu').addClass('clearfix');
			}	
		});

		/**************************
			Reorder Footer Links
		***************************/
		reorder_links();
		$(window).resize(function(){
			clearTimeout(window.resizedReorder);
			window.resizedReorder = setTimeout(reorder_links, 150);
		});
	};


	function menu_toggle(){					
		$('body').on('click', '.footer__col', function(e){
			var $this = $(this),
				icon = $this.children('.col-title--wrapper').children('.glyphicon');

			/*****************************
				Toggle submenu display
			******************************/
			if( !$this.hasClass('footer__social') ){
				// Hide currently displayed submenu's
				$('.footer__col ul').not( $this.children('ul') ).removeClass('show');
				$('.footer__col .social').removeClass('show');
				$('.email_upd--footer').removeClass('show');
				
				//toggle clicked element submenu
				$this.children('ul').toggleClass('show');					
			} else {
				// Hide currently displayed submenu's
				$('.footer__col ul').not( $this.children('ul') ).removeClass('show');
				
				// toggle social, email input
				$this.children('.social').toggleClass('show');
				$('.email_upd--footer').toggleClass('show');															
			}

			/*****************************
				Toggle Glyphicon
			******************************/
			if( icon.hasClass('glyphicon-triangle-right') ){					
				// Toggle currently displayed glyphs
				$('.glyphicon-triangle-bottom')
					.not(icon)
					.removeClass('glyphicon-triangle-bottom')
					.addClass('glyphicon-triangle-right');

				// Toggle clicked element glyph
				icon.removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
			} else {
				icon.removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
			}
		});	
	}


	function reorder_links(){
		if( $(window).width() < 832 ){
			$('.priv_pol, .terms_cond').prependTo('.footer__links ul');
		} else if( $('.priv_pol').is(':first-child') ){
			$('.copyright').prependTo('.footer__links ul');
		}
	}


	return {
		init: init
	}



})();