var Carousels = (function(){

	var init = function(){
		// Init carousels
		$('.carousel').carousel({
			interval: false
		});
		
		$(".carousel").swiperight(function() {  
			$(this).carousel('prev');  
		});  
		$(".carousel").swipeleft(function() {  
			$(this).carousel('next');  
		});  
	};


	return {
		init: init
	}

})();