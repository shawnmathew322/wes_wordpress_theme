$(document).ready(function(){
	
	// Populate Year
	var currentDate = new Date(),
		year = currentDate.getFullYear();

	$('.year').text(year);

	//Global
	Nav.init();
	Search.init();
	Carousels.init();
	Email_Input.init();
	Footer.init();

	//Homepage
	Header.init();
	
	//Events
	Filter_Events.init();


});