var Nav = (function(){

	var nav_is_fixed,
		$mBar = $(".nav .mobile .bar"),
		$mMenu = $(".nav__mobile"),

		ua = navigator.userAgent.toLowerCase(),
		isAndroid = ( ua.indexOf('android') > -1 ) ? true : false,
		iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,

		currentScrollPos;

		if( iOS ){
			$(window).scroll(function(){
				currentScrollPos = $(document).scrollTop();
			});			
		}


	var init = function(){		

		/****************************
			Fixed Nav
		*****************************/
		checkMobileNav();
		$(window).scroll(function(){
			checkMobileNav();
		});		


		/****************************
			Top Nav Click
		*****************************/
		BindDropClick();


		/****************************
			Main Nav Click
		*****************************/		
		main_nav_click();


		/*****************************
			Hide Dropdowns, Searchform on Resize
		*****************************/		
		$(window).resize(function(){
			$(".main--drop").removeClass("drop--open");
			$(".nav__main ul li ul").hide();

			//hide menus & search	
			if( !isAndroid ){											
				$(".nav__search__container").hide();				
			} else {
				if( !$('.nav--search').is(':focus') ){
					$(".nav__search__container").hide();
				}
			}
		});
				

		/****************************
			Mobile Click 
		*****************************/		
		mobile_click_events();

	};



	function main_nav_click(){
		$(".main-nav-item").on("click",function(){
			var $item = $(this).parent().find("ul").eq(0);
			$(".nav__main ul li ul").not($item).hide();
			$item.toggle();			
			
			//hide search
			$(".nav__search__container").hide();			
		});

		$(document).on('click', function(e){
			if( $(e.target).parents('.nav__main').length == 0 ){
				$(".nav__main ul li ul").hide();
			}
		});	
	}	
		

	function checkMobileNav(){
		var sTop = $(window).scrollTop();
		if(sTop >= 114 && !$mBar.hasClass("mobile--top")){
			$mBar.addClass("mobile--top");
			$mMenu.addClass("mobile--top");

			nav_is_fixed = true;

			if( $('.nav__mobile').is(':visible') ){
				$('html, body').addClass('no_scroll');
				$('.main_wrapper').addClass('opaque--nav_display');
			}			
		}
		else if(sTop < 114 && $mBar.hasClass("mobile--top")){
			$mBar.removeClass("mobile--top");
			$mMenu.removeClass("mobile--top");

			nav_is_fixed = false;
			
			if( $('.nav__mobile').is(':visible') ){
				$('html, body').removeClass('no_scroll');						
				$('.main_wrapper').removeClass('opaque--nav_display');
			}			
		}
	}


	function BindDropClick(){	
		// Display sub link
		$('body').on('click', '.main--drop--selected', function(){			
			var sub_link = $(this).siblings('.main--drop--option');
			
			if( $('.main--drop--option').not(sub_link).hasClass('show') ){
				$('.main--drop--option').removeClass('show')
			}

			sub_link.toggleClass('show');
		});		

		// Sub link click
		$('body').on('click', '.main--drop--option.show', function(){
			var orig_display_link = $(this).parent().children('.main--drop--selected');

			 orig_display_link.removeClass('main--drop--selected').addClass('main--drop--option');
			 $(this).removeClass('main--drop--option show').addClass('main--drop--selected');			
		});

		// Hide Sub link on off click
		$(document).on('click', function(e){
			if( e.target.className !== 'main--drop--selected' ){
				$('.main--drop--option').removeClass('show');
			} 
		});
	}
	

	function mobile_click_events(){
		// Hamburger Click
		$('body').on("click", ".hamburger", function(){
			// Toggle Nav Display
			$(".nav__mobile").toggle();
			$(this).toggleClass("openNav");

			// Prevent body scroll && iOS specific logic
			if( $(this).hasClass('openNav') ){				
				$('.main_wrapper').addClass('opaque--nav_display');

				if( nav_is_fixed ){
					$('html, body').addClass('no_scroll');				
				}

				if( iOS ){
					$('.main_wrapper').addClass('opaque--nav_display');
					removeIOSRubberEffect();
				}
			} else {
				$('html, body').removeClass('no_scroll');				
				$('.main_wrapper').removeClass('opaque--nav_display');
			}
			
		});
		
		// Nav Item Click
		$(".nav__mobile a.drop").on("click",function(e){
			e.preventDefault();

			var $drop = $(this).parent().find("ul").eq(0);
			$drop.toggle();
			$(".nav__mobile ul ul").not($drop).hide();
			$(this).toggleClass("openDrop");
			$("a.drop").not($(this)).removeClass("openDrop");
		});
	}


	function removeIOSRubberEffect() {
     	$('.scrollable').on('touchstart', function(){
     		var top = $(this).scrollTop,
     			totalScroll = $(this).scrollHeight,
     			currentScroll = top + $(this).offsetHeight;
 
 			if ( top === 0 ) {
 	            $(this).scrollTop = 1;
 	        } else if ( currentScroll === totalScroll ) {
 	            $(this).scrollTop = top - 1;
 	        }
     	});
 	}


 	/***********************
 		TEMP REMOVED - do not use as fn call
 	************************/ 
 	function ios_prevent_scrolling(){
 		if( iOS ){
			if( $(this).hasClass('openNav') ){
				$('.logo--mobile').hide();				

				$('.main_wrapper')
					.css({
						'position': 'fixed'
					})
					.addClass('opaque--nav_display');

				removeIOSRubberEffect();

				localStorage.setItem('cachedScrollPos', currentScrollPos);
			} else {				
				$('.logo--mobile').show();
				$('.main_wrapper')
					.css({					
						'position': 'relative'
					})
					.removeClass('opaque--nav_display');
				
				$('body').scrollTop( localStorage.getItem('cachedScrollPos') );
			}
		}		
 	}


	return {
		init: init
	}

})();



;
var Carousels = (function(){

	var init = function(){
		// Init carousels
		$('.carousel').carousel({
			interval: false
		});
		
		$(".carousel").swiperight(function() {  
			$(this).carousel('prev');  
		});  
		$(".carousel").swipeleft(function() {  
			$(this).carousel('next');  
		});  
	};


	return {
		init: init
	}

})();;
var Email_Input = (function(){

	var $email_events = $('.email_upd--events .wpcf7-form input[type="email"]'),
		$email_footer = $('.email_upd--footer .wpcf7-form input[type="email"]');

	var init = function(){

		// Email Input form
		email_input_size()		

		$(window).resize(function(){
			email_input_size()			
		});		

	};


	function email_input_size(){
		// Set initial email footer input size
		$email_footer.attr('size', '20');

		if( $(window).width() > 1380 ){
			$email_events.attr('size', '40');				
		} else if( $(window).width() < 1380 && $(window).width() > 1140 ){
			$email_events.attr('size', '30');		
		} else {
			$email_events.attr('size', '20');			
		}
	};

	return {
		init: init
	}

})();;
var Footer = (function(){

	var init = function(){
		
		/**************************
			Footer Menu Dropdowns
		***************************/
		menu_toggle();
		$(window).resize(function(){
			if( $(window).width() < 832 ){					
				// Remove clear from div to prevent extra spacing
				$('.footer__menu').removeClass('clearfix');

				// Hide any previously displayed sub menu's & toggle glyphs
				if( $('.footer__col ul').hasClass('show') ){
					$('.footer__col ul').removeClass('show')
				}

				if( $('.col-title--wrapper > .glyphicon').hasClass('glyphicon-triangle-bottom') ){
					$('.col-title--wrapper > .glyphicon')
						.removeClass('glyphicon-triangle-bottom')
						.addClass('glyphicon-triangle-right');
				}
			} else {
				// Add back clear on footer menu div
				$('.footer__menu').addClass('clearfix');
			}	
		});

		/**************************
			Reorder Footer Links
		***************************/
		reorder_links();
		$(window).resize(function(){
			clearTimeout(window.resizedReorder);
			window.resizedReorder = setTimeout(reorder_links, 150);
		});
	};


	function menu_toggle(){					
		$('body').on('click', '.footer__col', function(e){
			var $this = $(this),
				icon = $this.children('.col-title--wrapper').children('.glyphicon');

			/*****************************
				Toggle submenu display
			******************************/
			if( !$this.hasClass('footer__social') ){
				// Hide currently displayed submenu's
				$('.footer__col ul').not( $this.children('ul') ).removeClass('show');
				$('.footer__col .social').removeClass('show');
				$('.email_upd--footer').removeClass('show');
				
				//toggle clicked element submenu
				$this.children('ul').toggleClass('show');					
			} else {
				// Hide currently displayed submenu's
				$('.footer__col ul').not( $this.children('ul') ).removeClass('show');
				
				// toggle social, email input
				$this.children('.social').toggleClass('show');
				$('.email_upd--footer').toggleClass('show');															
			}

			/*****************************
				Toggle Glyphicon
			******************************/
			if( icon.hasClass('glyphicon-triangle-right') ){					
				// Toggle currently displayed glyphs
				$('.glyphicon-triangle-bottom')
					.not(icon)
					.removeClass('glyphicon-triangle-bottom')
					.addClass('glyphicon-triangle-right');

				// Toggle clicked element glyph
				icon.removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
			} else {
				icon.removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
			}
		});	
	}


	function reorder_links(){
		if( $(window).width() < 832 ){
			$('.priv_pol, .terms_cond').prependTo('.footer__links ul');
		} else if( $('.priv_pol').is(':first-child') ){
			$('.copyright').prependTo('.footer__links ul');
		}
	}


	return {
		init: init
	}



})();;
var Header = (function(){

	var ua = navigator.userAgent.toLowerCase(),
		isAndroid = ( ua.indexOf('android') > -1 ) ? true : false;


	var init = function(){
		// Apply Now
		$('.js_apply_now').on('click', function(){
			$('.apply-now').show();
			prevent_body_scroll();
		});

		$('.close_apply').on('click', function(){
			$('.apply-now').hide();
			$('html, body').removeClass('no_scroll')
			$(document).unbind('touchmove');
		});

		// Toggle scroll prevention depending on width & device
		$(window).resize(function(){
			if( $(window).width() > 959 && $('html, body').hasClass('no_scroll') ){
				$('html, body').removeClass('no_scroll');				
				$(document).unbind('touchmove');
			} else {
				if( $('.apply-now').is(':visible') ){
					prevent_body_scroll();
				}
			}
		});
	};


	function prevent_body_scroll(){
		if( $(window).width() < 960 ){
			$('html, body').addClass('no_scroll');

			if( isAndroid ){
				$(document).on('touchmove', function(e){
					e.preventDefault();
				});
			}
		}		
	}


	return {
		init: init
	}



})();;
var Search = (function(){

	var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;


	var init = function(){
		// Toggle Search Display
		toggle_search();	

		// Resize - on desktop, remove opaque display		
		$(window).resize(function(){
			if( $(window).width() > 959 ){
				$(".mobile__search__box").hide();	
				$('header, .banner, section, footer').hasClass('opaque') ? $('header, .banner, section, footer').removeClass('opaque') : null;						
			}
		});
	};


	function toggle_search(){

		$(".nav__search, .nav--search--close .btn").on("click",function(){
			$(".nav__search__container").toggle();
			// hide nav drop downs
			$(".nav__main ul li ul").hide();
		});

		$(".mobile__search, .mobile--search--close").on("click",function(){
			$(".mobile__search__box").toggle();
			on_search_display();

			// Prevent body scrolling
			$('html, body').toggleClass('no_scroll');			
			if( iOS ){
				if( $('.mobile__search__box').is(':visible') ){
					$(document).on('touchmove', function(e){
						e.preventDefault();
					});		
				} else {
					$(document).unbind('touchmove');
				}		
			}
		});
	}


	function on_search_display(){
		$('header, .banner, section, footer').toggleClass('opaque');
	}

	return {
		init: init
	}


})();;
var Filter_Events = (function(){

	var events = $('.event__item'),
		filter_terms_obj = {},
		filter_terms_arr = [],
		filter_terms = '',
		remove_term = false,
		filter_type = '',
		multi_term_type_filter = false,
		filter_type_count = 1;

	var init = function(){			

		// Prevent Selection of both countries
		$('[data-filter="country"]').on('click', function(e){
			if( $('[data-filter="country"]:checked').length > 1 ){
				e.preventDefault();
			} else {
				return;
			}
		});

		// Checkbox Filter
		$('.filter input[type="checkbox"]').on('change', function(){			

			var filter_value = $(this).attr('value'),
				filter_type = $(this).data('filter');

			if( $(this).is(':checked') ){
				remove_term = false;
				filter_sort(filter_value, filter_type);
			} else {
				remove_term = true;
				filter_sort(filter_value, filter_type);
			}
						
			events_sort();
		});		

		// Keyword Filter
		$('.tag').on('click', function(){
			// Get Value for Sort
			var filter_value = $(this).data('filter'),
				filter_type = $(this).data('filter');
			
			// Select Checkbox, Trigger Change Event
			$('[value="' + filter_value + '"]').prop('checked', true).trigger('change');
			
		});

		// Clear Filters
		$('.clear_filter').on('click', function(){
			$('.filter input[type="checkbox"]').prop('checked', false);
			filter_terms = '';
			events_sort();

			multi_term_type_filter = false;
		});
 
	};


	function filter_sort(filter_value, filter_type){		

		filter_value = '.' + filter_value;		

		if( remove_term == false ){												
			update_filter_terms_arr(filter_value, filter_type);
			update_filter_terms();
		} else {
			update_filter_terms_arr(filter_value, filter_type);
			update_filter_terms();
		}

		console.log(filter_terms);
	}


	function update_filter_terms_arr(filter_value, filter_type){				

		// Add to filter_terms		
		if( remove_term == false ){

			// if filter terms !empty - append current filter value to each filter term based on filter type
			if( filter_terms_arr.length > 0 ){
				// Determine if multi type, multi term filter by 
				// checking if obj props != current filter type have values
				for(var prop in filter_terms_obj){
					if( prop !== filter_type ){
						if( filter_terms_obj[prop].length > 0 ){
							multi_term_type_filter = true;
							filter_type_count++;
						}
					}
				}

				// if filter_terms_obj has filter_type property
				if( filter_terms_obj.hasOwnProperty(filter_type) ){
					add_filter_terms_arr(filter_value, filter_type);
				}
				// If filter_terms_obj does not have filter_type property 
				else {
					filter_terms_arr.forEach(function(term, index, filter_terms_arr){
						filter_terms_arr[index] = filter_value + term;				
					});
				}						
			} else {
				// filter_terms = empty, just add current filter value
				
				// Update Terms Array
				filter_terms_arr.push(filter_value);	
			}

			// Add Update Terms Object
			update_filter_terms_obj(filter_value, filter_type);
		} 

		// Remove from filter_terms
		else {
			// Remove Value from Terms Object Property
			update_filter_terms_obj(filter_value, filter_type);

			// Remove Term from Terms Array
			remove_filter_terms_arr(filter_value, filter_type);

		}// remove from filter_terms
		
		console.log(filter_terms_obj);

	}// update_filter_terms_arr


	/****************************************************
	 Update filter terms array based on filter 
	 term object property values & if multi term filter
	*****************************************************/
	function add_filter_terms_arr(filter_value, filter_type){
		// If Multi Term & Current Filter Type object property has values
		if( filter_terms_obj[filter_type].length > 0 && multi_term_type_filter ){			
			if( filter_type_count > 2 ){
				filter_terms_arr.forEach(function(term){
					// If term contains value from current filter value's filter type
					if( term.indexOf(filter_terms_obj[filter_type][0]) > -1 ){
						// Add a new term by replacing first instance of shared filter type value
						addNewTerm = term.replace(filter_terms_obj[filter_type][0], filter_value);
						filter_terms_arr.push( addNewTerm );
					}
				});				
			} else {
				for(var prop in filter_terms_obj){							
					if( prop !== filter_type ){
						filter_terms_obj[prop].forEach(function(el){
							filter_terms_arr.push( filter_value + el );
						});																
					}
				}	
			}						
		} else if( filter_terms_obj[filter_type].length < 1 && multi_term_type_filter ){
			// If Multi Term & Current Filter Type object property does not have values
			filter_terms_arr.forEach(function(term, index, filter_terms_arr){
				filter_terms_arr[index] = filter_value + term;				
			});	
		} else {
			// If !multi-term, add current filter value to terms array
			filter_terms_arr.push(filter_value);
		}	
	};


	/****************************************************
	 Remove Term From Filter Terms Array
	*****************************************************/
	function remove_filter_terms_arr(filter_value, filter_type){
		var remove_filter_terms = [],
			is_single_type = false;

		// Check value against each term in fiter_terms_arr & update filter_terms_arr			
		filter_terms_arr.forEach(function(term, index, filter_terms_arr){
			if( term.indexOf(filter_value) > -1 ){
				
				// Set var for check if reduced terms are all in one filter type		
				for(var prop in filter_terms_obj){
					// Check current filter_value type array length						
					if( filter_terms_obj[filter_type].length < 1 ){
						// Check if other filter types are populated
						if( prop !== filter_type ){
							if( filter_terms_obj[prop].length > 0 ){							
								is_single_type = true;
								multi_term_type_filter = false;			
								filter_type_count = 1;						
							}	
						}	
					}						
				}

				// Check if filter terms are still multi type
				if( !is_single_type ){
					remove_filter_terms.push(term);
				} else {
					filter_terms_arr[index] = term.replace(filter_value, '');
				}										
			}// term.indexOf(filter_value)												
		});			

		// If multi type filter, splice filter_terms_arr using remove terms values
		if( !is_single_type ){
			for( var i = remove_filter_terms.length - 1; i >= 0; i-- ){
				var remove_term_indx = filter_terms_arr.indexOf(remove_filter_terms[i]);
				filter_terms_arr.splice( remove_term_indx, 1 );
			}	
		}
	};


	/****************************************************
	 Add to filter terms object 	 
	*****************************************************/
	function update_filter_terms_obj(filter_value, filter_type){
		// Add to Terms Object
		if( remove_term == false ){
			if( !filter_terms_obj.hasOwnProperty(filter_type) ){
				filter_terms_obj[filter_type] = [];				
			}
			filter_terms_obj[filter_type].push(filter_value);

		} else {
			// Remove
			var upd_obj = filter_terms_obj[filter_type].indexOf(filter_value);
			filter_terms_obj[filter_type].splice(upd_obj, 1);
		}			
	};


	/****************************************************
	 Update filter terms string 	 
	*****************************************************/
	function update_filter_terms(){
		filter_terms = '';
		
		filter_terms_arr.forEach(function(term, index){						
			filter_terms += ( filter_terms !== '' ) ? ',' + term : term;			
		});
	};


	/****************************************************
	 Sort event items
	*****************************************************/
	function events_sort(){
		events.fadeOut('fast');

		if( filter_terms !== '' ){
			events.filter(filter_terms).fadeIn('slow');	
		} else {
			events.fadeOut('fast').fadeIn('slow');
		}
	};



	return {
		init: init
	}


})();;
$(document).ready(function(){
	
	// Populate Year
	var currentDate = new Date(),
		year = currentDate.getFullYear();

	$('.year').text(year);

	//Global
	Nav.init();
	Search.init();
	Carousels.init();
	Email_Input.init();
	Footer.init();

	//Homepage
	Header.init();
	
	//Events
	Filter_Events.init();


});