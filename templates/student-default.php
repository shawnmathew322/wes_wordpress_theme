<?php 
/*
Template Name: Student Default Template
*/

get_header(); 

?>


<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<div class='gen_page'>

		<?php get_template_part('/includes/student/default-template/intro'); ?>

		<?php get_template_part('/includes/student/default-template/main_content'); ?>
							
	</div><!-- gen_page -->

<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>


<?php get_footer(); ?>